$(document).ready(function(){

    $('.slider-content').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 2000,
        appendArrows: $('.slider-btn .container'),
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-circle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-circle-right"></i></button>',
        pauseOnHover: false
    });

    (function($){
        $(function() {
            $('.navigation-icon').on('click', function() {
                $(this).closest('.navigation')
                    .toggleClass('navigation-state-open');
                console.log(123)
            });

            $('.navigation-links-item').on('click', function() {
                // do something

                $(this).closest('.navigation')
                    .removeClass('navigation-state-open');
            });
        });
    })(jQuery);
});